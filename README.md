# lhe_like_events

Script to read events created by TEG version 1.0

The format of the events is similar to that of MadGraph

========================================
PDG code, in/out state, px, py, pz, E, m
========================================

PDG codes are     